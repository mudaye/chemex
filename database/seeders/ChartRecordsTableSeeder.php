<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ChartRecordsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('chart_records')->delete();


    }
}
