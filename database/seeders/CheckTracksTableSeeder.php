<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CheckTracksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('check_tracks')->delete();

        \DB::table('check_tracks')->insert(array(
            0 =>
                array(
                    'id' => 1,
                    'check_id' => 1,
                    'item_id' => 1,
                    'status' => 1,
                    'checker' => 1,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-27 12:22:10',
                ),
            1 =>
                array(
                    'id' => 2,
                    'check_id' => 1,
                    'item_id' => 2,
                    'status' => 0,
                    'checker' => 0,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-24 09:33:49',
                ),
            2 =>
                array(
                    'id' => 3,
                    'check_id' => 1,
                    'item_id' => 3,
                    'status' => 0,
                    'checker' => 0,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-24 09:33:49',
                ),
            3 =>
                array(
                    'id' => 4,
                    'check_id' => 1,
                    'item_id' => 4,
                    'status' => 0,
                    'checker' => 0,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-24 09:33:49',
                ),
            4 =>
                array(
                    'id' => 5,
                    'check_id' => 1,
                    'item_id' => 5,
                    'status' => 0,
                    'checker' => 0,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-24 09:33:49',
                ),
            5 =>
                array(
                    'id' => 6,
                    'check_id' => 1,
                    'item_id' => 6,
                    'status' => 0,
                    'checker' => 0,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-24 09:33:49',
                ),
            6 =>
                array(
                    'id' => 7,
                    'check_id' => 1,
                    'item_id' => 7,
                    'status' => 0,
                    'checker' => 0,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-24 09:33:49',
                ),
            7 =>
                array(
                    'id' => 8,
                    'check_id' => 1,
                    'item_id' => 8,
                    'status' => 0,
                    'checker' => 0,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-24 09:33:49',
                ),
            8 =>
                array(
                    'id' => 9,
                    'check_id' => 1,
                    'item_id' => 9,
                    'status' => 0,
                    'checker' => 0,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-24 09:33:49',
                ),
            9 =>
                array(
                    'id' => 10,
                    'check_id' => 1,
                    'item_id' => 10,
                    'status' => 0,
                    'checker' => 0,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-24 09:33:49',
                ),
            10 =>
                array(
                    'id' => 11,
                    'check_id' => 1,
                    'item_id' => 11,
                    'status' => 0,
                    'checker' => 0,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-24 09:33:49',
                ),
            11 =>
                array(
                    'id' => 12,
                    'check_id' => 1,
                    'item_id' => 12,
                    'status' => 0,
                    'checker' => 0,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-24 09:33:49',
                ),
            12 =>
                array(
                    'id' => 13,
                    'check_id' => 1,
                    'item_id' => 13,
                    'status' => 0,
                    'checker' => 0,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-24 09:33:49',
                ),
            13 =>
                array(
                    'id' => 14,
                    'check_id' => 1,
                    'item_id' => 14,
                    'status' => 0,
                    'checker' => 0,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-24 09:33:49',
                ),
            14 =>
                array(
                    'id' => 15,
                    'check_id' => 1,
                    'item_id' => 15,
                    'status' => 0,
                    'checker' => 0,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-24 09:33:49',
                ),
            15 =>
                array(
                    'id' => 16,
                    'check_id' => 1,
                    'item_id' => 16,
                    'status' => 0,
                    'checker' => 0,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-24 09:33:49',
                ),
            16 =>
                array(
                    'id' => 17,
                    'check_id' => 1,
                    'item_id' => 17,
                    'status' => 0,
                    'checker' => 0,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-24 09:33:49',
                ),
            17 =>
                array(
                    'id' => 18,
                    'check_id' => 1,
                    'item_id' => 18,
                    'status' => 0,
                    'checker' => 0,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-24 09:33:49',
                ),
            18 =>
                array(
                    'id' => 19,
                    'check_id' => 1,
                    'item_id' => 19,
                    'status' => 0,
                    'checker' => 0,
                    'description' => NULL,
                    'deleted_at' => NULL,
                    'created_at' => '2021-03-24 09:33:49',
                    'updated_at' => '2021-03-24 09:33:49',
                ),
        ));


    }
}
